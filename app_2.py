from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/', methods=['POST'])
def main():
    message = {'status': 1,
               'message': 'Welcome to Server 2!',
               'code': 200,
               'data': {}}
    print(request.json)
    print(request.headers)

    resp = jsonify(message)
    return resp

if __name__ == '__main__':
    app.run("0.0.0.0", port = 3001, threaded=False, debug=True)

