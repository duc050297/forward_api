import os
import requests
import threading
from flask import Flask, jsonify, make_response, request
from flask_cors import CORS, cross_origin
from telebot import TeleBot
from dotenv import load_dotenv

load_dotenv()
ACCESS_KEY = os.environ.get('ACCESS_KEY')
HN_INTERNAL_API = os.environ.get('HN_INTERNAL_API')
HCM_INTERNAL_API = os.environ.get('HCM_INTERNAL_API')
BOT_TOKEN = os.environ.get('BOT_TOKEN')
CHAT_ID = os.environ.get('CHAT_ID')


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
bot = TeleBot(BOT_TOKEN)

@app.route('/', methods=['POST'])
def main():
    message = {'status': 1,
               'message': 'Welcome to Server!',
               'code': 200,
               'data': {}}
    access_key = request.headers.get('Access-Key')
    if access_key != ACCESS_KEY:
        resp = make_response({"message": "Invalid access key!"}, 403)
        return resp
    
    location = request.get_json().get('location')

    try:
        if location == 'hanoi':
            requests.post(HN_INTERNAL_API, data=request.data, headers=request.headers)
        elif location == 'hcm':
            requests.post(HCM_INTERNAL_API, data=request.data, headers=request.headers)
    except Exception as ex:
        message_template = "Lỗi rồi"
        thread = threading.Thread(target=bot.send_message, args=(CHAT_ID, message_template,))
        thread.start()
        
    resp = jsonify(message)
    return resp

if __name__ == '__main__':
    app.run("0.0.0.0", port = 3000, threaded=False, debug=True)

